# Deploy remote

The script `deploy-remote.sh` allows to build something and deploy an artifact to remote, in one script.

## Usage

```
./deploy-remote.sh
```

Then follow the instructions and export the needed variables in your shell before the execution.

## Example to build and deploy static websites with Jekyll

```
export REPOSITORY_URL="https://gitlab.com/example/example.git"
export REMOTE_BRANCH="master"
export BUILD_COMMAND="bundle exec jekyll build"
export LOCAL_REPO_DIR="/tmp/mysite"
export SOURCE="_site"
export DESTINATION="root@example.com:/var/www/production"
./deploy-remote.sh
```
