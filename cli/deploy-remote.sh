#!/bin/bash
########################################################################
# deploy-remote.sh - Your stupid "git CI/CD" script
########################################################################
#
# DESCRIPTION
#
# This script is useful for project maintainers and/or automation
# scripts to clone/pull a remote git source code, do something, and
# copying something somewhere.
#
# For example you can build some static HTML files from Jekyll and them
# upload them to a server, over SSH.
#
# This script is NOT useful to end-users.
#
# In every case, this script will do its best to ignore any local change
# and work on a clean fresh copy obtained from remote, and then restore
# any weird uncommitted shit and/or any committed local change, from
# whatever local branch you were working locally.
#
########################################################################
#
# WORKFLOW
#
# - create temporary branch
# - git pull origin
# - build
# - send something somewhere
#
# This "branch over-complication" is very useful because most people
# like to do very WEIRD things on their local git copy and shit WILL
# SURELY happens, but we really have NO time to listen to any of your
# "OH MY GOD THIS DAMN SCRIPT HAS TOUCHED A LOCAL FILE AAAAAAH"
# so, since we are pro-active, this script never deletes your local
# shit, even if you try to do very weird local stuff.
#
# It works. You're welcome. -boz
########################################################################
#
# EXAMPLE USAGE
#
#     export REPOSITORY_URL="https://gitlab.com/example/example.git"
#     export REMOTE_BRANCH="master"
#     export LOCAL_REPO_DIR="/tmp/mysite"
#     export BUILD_COMMAND="bundle exec jekyll build"
#     export SOURCE="_site"
#     export DESTINATION="root@example.com:/var/www/mysite"
#     /opt/ils-infrastructure/cli/deploy-remote.sh
#
########################################################################
#
# SUPPORTED USE-CASES
#
# Recommended use case "I love automation":
# - A contributor: commit, create pull request
# - B maintainer:  merge on master
# - C bot:         run this script
#                  «Oh wow! such automation asd»
#
# Another supported use case "I hate automation"
# - A contributor: commit, create pull request
# - B maintainer:  merge on master, run this script
#                  «Oh wow! such manual power asd»
#
# Another supported use case "I love myself"
# - A maintainer:  commit on master, run this script
#                  «Oh wow! chi fa da se' fa per tre asd»
#
# Another supported use case "Don't stop me now":
# - A contributor: commit, create pull request
# - B maintainer:  has broken/weird local changes, merge on master, run this script
#                  «Oh wow! only 'A' is online and 'B' can continue to work»
#
# There are surely even-more already-supported stupid use-cases
# not already mentioned there. Feel free to add.
########################################################################
#
# (C) 2022 Valerio Bozzolan and contributors
# License: MIT
#
########################################################################

# exit in case of any error
set -e

#
# Remote git repository URL
#
if [ -z "$REPOSITORY_URL" ]; then
	echo "[FAIL] Please export a var REPOSITORY_URL containing a valid git repository (example: https://example.com/example.git)"
	exit 1
fi

#
# Interested remote git $branch used to pull changes
#
if [ -z "$REMOTE_BRANCH" ]; then
	REMOTE_BRANCH=master
	echo "[WARN] Please export a var REMOTE_BRANCH containing a valid remote branch name (example: master) (example: main)"
	echo "[WARN]                     REMOTE_BRANCH assumed as $REMOTE_BRANCH"
fi

#
# Local pathname on your computer used to run "git clone" into
#
if [ -z "$LOCAL_REPO_DIR" ]; then
	echo "[FAIL] Please export a var LOCAL_REPO_DIR containing a writable git repository (existing or not)"
	exit 1
fi

#
# Compilation command
#
if [ -z "$BUILD_COMMAND" ]; then
	echo "[FAIL] Please export a var BUILD_COMMAND with the exact command that will be executed after git pull"
	echo "[TIP ] The BUILD_COMMAND will be executed inside LOCAL_REPO_DIR ($LOCAL_REPO_DIR)"
	exit 1
fi

#
# Source files to be copied
#
# it MUST exist after your build
#
if [ -z "$SOURCE" ]; then
	echo "[FAIL] Please export a var SOURCE containing a valid source pathname (example: compiledDirectory/)"
	echo "[TIP ] If SOURCE is a directory, add a final slash to just copy its content, or none to copy the dir itself"
	exit 1
fi

#
# Destination for your static website
#
# it MUST exist
# it SHOULD be a valid SSH destination
#
if [ -z "$DESTINATION" ]; then
	echo "[FAIL] Please export a var DESTINATION containing a valid SSH destination (example: root@example.com:/var/www/public_html/)"
	exit 1
fi

#
# Local temporary branch name
#
# This will be created to avoid conflicts with your current branch (master?).
#
if [ -z "$LOCAL_TMP_BRANCH" ]; then
	# assume a nice default
	LOCAL_TMP_BRANCH="tmp-build-deploy"
fi

#
# Check if the user is doing nonsense stuff
#
if [ "$REMOTE_BRANCH" = "$LOCAL_TMP_BRANCH" ]; then
	echo "[FAIL] The variables REMOTE_BRANCH and LOCAL_TMP_BRANCH must differ"
	exit 1
fi

# check if we need to do the very first git clone
if ! [ -d "$LOCAL_REPO_DIR" ]; then

	# create the local copy and assign it to "origin"
	git clone --origin origin -- "$REPOSITORY_URL" "$LOCAL_REPO_DIR"
fi

# enter in the git repository
echo "[INFO] Enter in $LOCAL_REPO_DIR"
cd "$LOCAL_REPO_DIR"

# check the name of the branch that was in use locally (if any)
# in order to know if/where the user was working locally
# so, after our build, we will kindly restore that branch as it was
PREVIOUS_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# check if it was possible to find the current branch
if [ -z "$PREVIOUS_BRANCH" ]; then

	PREVIOUS_BRANCH="$REMOTE_BRANCH"
	echo "[WARN] Unable to detect current branch. Assuming $PREVIOUS_BRANCH"
fi

# check if the user already was on the temporary branch
if [ "$PREVIOUS_BRANCH" = "$LOCAL_TMP_BRANCH" ]; then

	PREVIOUS_BRANCH="$REMOTE_BRANCH"
	echo "[WARN] Why were you surfing in my branch $LOCAL_TMP_BRANCH? Uhm? UHM?"
	echo "[WARN] When we will conclude I will take you back to $PREVIOUS_BRANCH"
fi

# check if there are local changes
VIRGIN_BRANCH=1
if [ -n "$(git status --porcelain)" ]; then

	echo "[INFO] Found local changes"
	VIRGIN_BRANCH=0
fi

# check if the branch was not clean
STASHED_SHIT=0
if [ "$VIRGIN_BRANCH" = 0 ]; then

	echo "[INFO] Your local changes will be ignored in this build and restored later"

	# NOTE: the git stash command "save" is deprecated
	#       and replaced by "push" but that is is too recent and sometime
	#       not available (for example is not available in git 2.11.0
	#       provided by Debian GNU/Linux buster
	git stash save --quiet --include-untracked

	STASHED_SHIT=1
fi

# create a local temporary branch from the desired remote branch
echo "[INFO] Taking clean branch $REMOTE_BRANCH and creating branch $LOCAL_TMP_BRANCH"
git checkout --quiet -B "$LOCAL_TMP_BRANCH" origin/"$REMOTE_BRANCH"

echo "[INFO] Pulling changes from origin $REMOTE_BRANCH"
git pull origin --quiet "$REMOTE_BRANCH"

# assume that everything is OK
# NOTE: this will be the exit status of this script
EPIC_FAIL=0

# build your application with your custom command BUILD_COMMAND (environment variable)
# NOTE: the current directory is LOCAL_REPO_DIR (also environment variable)
echo "[INFO] Start your build command"
if ! $BUILD_COMMAND; then

	echo "[FAIL] Your build failed with exit status: $?"
	echo "[FAIL]   Note that the current directory is: $(pwd)"
	echo "[FAIL]   Here the problematic command (as you defined in BUILD_COMMAND):"
	echo "[FAIL]     $BUILD_COMMAND"
	EPIC_FAIL=1
else
	echo "[INFO] Build command concluded successfully!"
fi

# no rsync no party
if ! which rsync > /dev/null; then

	# in theory this is never executed since you already have rsync
	# assume that you have Debian/Ubuntu
	sudo apt-get update
	sudo apt-get install --yes rsync
fi

# try to transfer files only if it has sense
if [ "$EPIC_FAIL" = 0 ]; then

	echo "[INFO] Start sending '$SOURCE' to '$DESTINATION' removing extra files"
	if ! rsync $RSYNC_EXTRA_FLAGS --recursive --links --delete "$SOURCE" "$DESTINATION"; then

		# show some useful info
		echo "[FAIL] The transfer of your local files to the remote server via SSH failed with exit status: $?"
		echo "[FAIL]   Check if the SOURCE directory is correct: '$SOURCE' (from current directory '$(pwd)')"
		echo "[FAIL]   Check if your DESTINATION is a valid SSH destination: '$DESTINATION'"
		echo "[FAIL]   Check if your local SSH configuration (~/.ssh/config) is OK for you"
		echo "[FAIL]   Check the specific rsync problem for example exporting RSYNC_EXTRA_FLAGS='-v'"
		EPIC_FAIL=1
	fi

fi

# NOTE: the current directory is:   LOCAL_REPO_DIR   (example: your git repository)
# NOTE: the current branch is:      LOCAL_TMP_BRANCH (example: "tmp-build-stuff")
# NOTE: the previous git branch is: PREVIOUS_BRANCH  (example: "master")

# turn back to the previously-active branch and remove my temporarly-created branch
if [ -n "$PREVIOUS_BRANCH" ]; then

	# we are inside our temporary branch
	# revert any local change caused by our compilation
	echo "[INFO] Cleaning branch $LOCAL_TMP_BRANCH"
	git checkout --quiet -- .

	echo "[INFO] Going back to branch $PREVIOUS_BRANCH"
	git checkout --quiet "$PREVIOUS_BRANCH"

	# check if there were local changes to be restored
	if [ "$STASHED_SHIT" = 1 ]; then

		echo "[INFO] Restoring your precious local changes"
		git stash pop --quiet
	fi

	echo "[INFO] Deleting branch $LOCAL_TMP_BRANCH"
	git branch --quiet -d "$LOCAL_TMP_BRANCH"

fi

# and now we can make this shit less boring
if [ "$EPIC_FAIL" = 0 ]; then
	echo "[INFO] FINAL NOTE: YOU ARE A WINNER! Build and transfer SUCCESSFULL!"
	echo "[INFO] (Looking at you I never would have bet on it! Good job!)"
else
	echo "[FAIL] FINAL NOTE: YOU ARE A LOSER! Something FAILED! See above"
	echo "[FAIL] (What a great day to consider a job change. Isn't it?)"
fi

# revert the "cd" that was executed before
# this is not really needed but it's nice to do it
cd - > /dev/null

# exit with something different than zero
exit "$EPIC_FAIL"
