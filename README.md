# Italian Linux Society Infrastructure

This repository contains some useful shared and public configuration files and scripts for Italian Linux Society's servers.

## Installation

This is just a simple git repository that should be placed in an Italian Linux Society server, and also on your computer to have reproducible builds.

Please install this repository in your `/opt` directory.

Suggested installation steps:

```
sudo -i
apt install --yes git
git clone https://gitlab.com/ItalianLinuxSociety/ils-infrastructure /opt/ils-infrastructure
```

## Update

Here the suggested update steps:

```
sudo -i
cd /opt/ils-infrastructure
git pull
```

## FAQ

You can surely install this repository anywhere you want, as long as you know what you are doing.

You can also not download it via root. But, if this repository is only writable by root, though, it's definitely safer. Especially in production.

## License

Copyright © 2022, 2023 Valerio Bozzolan, contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
