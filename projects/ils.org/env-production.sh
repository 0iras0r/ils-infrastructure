#!/bin/sh
########################################################################
# Environment file describing the deploy of www.ILS.org in production
########################################################################
# DESCRIPTION
#
# This is a public and versioned file containing
# some environment variables useful to deploy this project:
#
#     https://www.ils.org
#
# From the active remote branch of its git repository.
########################################################################
# USAGE
#
# This script loads all the environment variables required by the script
# called "deploy-remote.sh" that build a project and upload compiled
# files to a server via SSH.
#
# The script "deploy-remote.sh" is in this same git repository.
#
# In short here how it should be used to build and deploy:
#
#     . env-ilsorg-production.sh \
#       && deploy-remote.sh
#
# In short you load the environment variables and you call the script.
# It just works and there is nothing more to say.
########################################################################
# IMPORTANT: DON'T PUT SECRETS IN THIS FILE!
#
# If you have any secret customization feel free to run something like
# this to define REMOTE_BRANCH=secret:
#
#     . env-ilsorg-production.sh \
#       && REMOTE_BRANCH=secret deploy-remote.sh
########################################################################
# LICENSE
#
# (C) 2022, 2023 Valerio Bozzolan and contributors
# MIT License
########################################################################

#
# Your repository on your local filesystem
#
export LOCAL_REPO_DIR="/tmp/ils.org"

#
# Remote git repository URL
#
export REPOSITORY_URL="https://gitlab.com/ItalianLinuxSociety/ils.org.git/"

#
# Interested remote git $branch used to pull changes
#
export REMOTE_BRANCH="master"

#
# Jekyll profile to be used
#
# This variable is read from "build-jekyll-docker.sh"
#
export JEKYLL_ENV=production

#
# Jekyll version to be used by Docker
#
# https://github.com/envygeeks/jekyll-docker/blob/master/README.md
#
# This variable is read from "jekyll-docker.sh"
#
export JEKYLL_VERSION=3.8

#
# Command to be executed after we have the remote source code
# This command will be executed from the LOCAL_REPO_DIR
#
export BUILD_COMMAND="/opt/ils-infrastructure/cli/jekyll-docker.sh jekyll build"

#
# Source Jekyll directory to be copied
#
# if this is a directory it MUST end with a slash
export SOURCE="_site/"

#
# Destination for your static website
#
# it MUST exist
# it SHOULD be a valid SSH destination
# note: the default username it's your username
#       if this is not OK for you, you can change this
#       line or change your ~/.ssh/config to put something
#       like this:
#
#           Host www.ils.org
#               User git-builder-ilsorg
#
export DESTINATION="www.ils.org:/home/ilsorg/public_html/"
