# ITGATE

Picard
- Server di posta

Worf
- Siti istituzionali (linux.it, ils.org...)

# HETZNER

VPS
- Servizi - servizi.linux.it
- LinuxSi - linuxsi.com
- Planet - planet.linux.it
- MERGE-it - merge-it.net
- Siti minori (e.g. sistemainoperativo.it)

# GARR

peertube-prod
- PeerTube - video.linux.it

discourse
- Forum - forum.linux.it

rocket-chat-1
- Matomo - stats.linux.it
- Mautic - crm.linux.it
